# tnt is not a tracker

This repo contains a front end for traxe, which is a tracker framework for haxe.

The UI here is made thanks to peote libs, which is an OpenGL UI lib for haxe.

There is a demo running here - https://jamesobfisher.gitlab.io/tnt/

## How to use the demo?!

Each column represents a sample. Press `return` to start/stop the sequencer. Notice the play markers moving. If a number is present on a row instead of a dot, a sample will be played when the play marker is in position.

Use arrow keys to move the cursor around. Press a number to write it under the cursor. This represents how loud a sample will be played. The numbers are hexadecimal, so `a` through to `f` is a valid entry as well.

To erase a number press `.`.

To move all numbers up or down a column, press `insert` or `delete`.

To decrease or increase a column length pres `[` or `]`.

## Status

It is just the beginning of this project and should be considered experimental/explorative.

The plans are for it to work cross platform, using grig for audio where possible, which is a collection of haxe audio libraries.

For now it is using a rather basic implementation of timing and audio using the web audio only.

## Dependencies

```
# UI
haxelib git peote-view https://github.com/maitag/peote-view
haxelib git peote-layout https://github.com/maitag/peote-layout
haxelib git peote-text https://github.com/maitag/peote-text

# Tracker
haxelib git ob.parameters https://gitlab.com/jamesobfisher/ob.parameters
haxelib git ob.seq https://gitlab.com/jamesobfisher/ob.seq
haxelib git ob.traxe https://gitlab.com/jamesobfisher/ob.traxe

```

## More ...

Peote libs core project is here - https://github.com/maitag/peote-view

Grig lives here - https://grig.tech/
