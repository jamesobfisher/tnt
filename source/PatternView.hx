import elements.ElementSimple;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Tracker;
import peote.text.FontProgram;
import peote.text.Line;

class PatternView extends TextWidget {
	var tracker:Tracker;
	var tracks:Array<Array<Array<Line<GlyphStylePacked>>>> = [];

	public var beatMarkers:Array<{element:ElementSimple, rowIndex:Int}> = [];

	public function new(display:TextWidgetLayout, glyphStyle:GlyphStylePacked, fontProgram:FontProgram<GlyphStylePacked>, tracker:Tracker) {
		super(display, glyphStyle, fontProgram, "", true);

		this.tracker = tracker;
		var xOffset = 0;
		for (t in tracker.tracks) {
			var marker = new ElementSimple(xOffset, y, Theme.ColumnWidth * t.lanes.length, Theme.ColumnHeight, Theme.Edge);
			marker.z = -150;
			beatMarkers.push({element: marker, rowIndex: 0});
			backgroundBuffer.addElement(marker);
			var track = [];
			for (l in t.lanes) {
				var lane = [];
				for (rowIndex in 0...l.numRows) {
					var chars = [for (r in l.formatRow(rowIndex)) r.char].join("");
					var style = styleRow(rowIndex);
					var line = fontProgram.createLine(chars, xOffset, glyphStyle.height * rowIndex, style);

					lane.push(line);
				}
				track.push(lane);
				xOffset += Std.int(l.numColumns * glyphStyle.width);
			}
			tracks.push(track);
		}
	}

	function styleRow(rowIndex:Int):GlyphStylePacked {
		return rowIndex % 4 == 0 ? Theme.glyphStylePack(Theme.Pattern2) : Theme.glyphStylePack(Theme.Pattern3);
	}

	public function focusCursor(cursor:NavigatorCursor) {
		var line = tracks[cursor.trackIndex][cursor.laneIndex][cursor.rowIndex];
		highlight.x = Std.int(line.x + (highlight.w * cursor.columnIndex));
		highlight.y = Std.int(line.y);
		backgroundBuffer.updateElement(highlight);
	}

	public function updateTrackerText(cursor:NavigatorCursor) {
		var line = tracks[cursor.trackIndex][cursor.laneIndex][cursor.rowIndex];
		var row = tracker.tracks[cursor.trackIndex].getLane(cursor.laneIndex).formatRow(cursor.rowIndex);
		for (i in 0...row.length) {
			var style:GlyphStylePacked = switch (row[i].char) {
				case ".": styleRow(cursor.rowIndex);
				case _: Theme.ColumnColors[row[i].column];
			}
			fontProgram.lineSetChars(line, row[i].char, i, style);
		}
		fontProgram.updateLine(line);
	}

	public function updateMarker(trackIndex:Int) {
		beatMarkers[trackIndex].rowIndex = tracker.tracks[trackIndex].playIndex;
		beatMarkers[trackIndex].element.y = Std.int(beatMarkers[trackIndex].rowIndex * Theme.ColumnHeight);
		backgroundBuffer.updateElement(beatMarkers[trackIndex].element);
	}

	public function resetMarkers() {
		for (b in beatMarkers) {
			b.rowIndex = 0;
			b.element.y = 0;
			backgroundBuffer.updateElement(b.element);
		}
	}
}
