package layoutable;

import peote.layout.ILayoutElement;
import peote.layout.LayoutContainer;
import peote.text.FontProgram;
import peote.text.Line;
import peote.view.Buffer;
import peote.view.Color;
import peote.view.Element;

class TextLineElement implements ILayoutElement implements Element {
	@color public var c:Color = 0x55ff00d0; // using propertyname "borderColor" as identifier for setColorFormula()

	@posX public var x:Int = 0;
	@posY public var y:Int = 0;
	@sizeX @varying public var w:Int;
	@sizeY @varying public var h:Int;

	@zIndex public var z:Int = 0;

	@custom("maskX") @varying public var maskX:Int = 0;
	@custom("maskY") @varying public var maskY:Int = 0;
	@custom("maskWidth") @varying public var maskWidth:Int = 0;
	@custom("maskHeight") @varying public var maskHeight:Int = 0;

	var display:LayoutElementDisplay;
	var isVisible:Bool = false;
	// var glyphStyle:GlyphStylePacked;
	// var fontProgram:FontProgram<GlyphStylePacked>;
	var textLine:Line<GlyphStylePacked>;

	public function new(display:LayoutElementDisplay, color:Color, text:String = "") {
		this.c = color;
		this.display = display;
		textLine = this.display.fontProgram.createLine("", x, y);
	}

	// ------------------ update, show and hide ----------------------

	public inline function update(layoutContainer:LayoutContainer) {
		x = Math.round(layoutContainer.x);
		y = Math.round(layoutContainer.y);
		z = Math.round(layoutContainer.depth);
		w = Math.round(layoutContainer.width);
		h = Math.round(layoutContainer.height);
		this.display.fontProgram.lineSetPosition(textLine, x, y);
		this.display.fontProgram.updateLine(textLine);
		if (layoutContainer.isMasked) { // if some of the edges is cut by mask for scroll-area
			maskX = Math.round(layoutContainer.maskX);
			maskY = Math.round(layoutContainer.maskY);
			maskWidth = maskX + Math.round(layoutContainer.maskWidth);
			maskHeight = maskY + Math.round(layoutContainer.maskHeight);
		} else { // if its fully displayed
			maskX = 0;
			maskY = 0;
			maskWidth = w;
			maskHeight = h;
		}
	}

	public inline function show() {
		isVisible = true;
		(display.textLineBuffer : Buffer<TextLineElement>).addElement(this);
	}

	public inline function hide() {
		isVisible = false;
		(display.textLineBuffer : Buffer<TextLineElement>).removeElement(this);
	}

	// ---------------- interface to peote-layout ---------------------

	public inline function showByLayout() {
		if (!isVisible)
			show();
	}

	public inline function hideByLayout() {
		if (isVisible)
			hide();
	}

	public inline function updateByLayout(layoutContainer:LayoutContainer) {
		// TODO: layoutContainer.updateMask() from here to make it only on-need

		if (isVisible) {
			if (layoutContainer.isHidden) // if it is full outside of the Mask (so invisible)
			{
				#if peotelayout_debug
				// trace("removed", layoutContainer.layout.name);
				#end
				hide();
			} else {
				update(layoutContainer);
				(display.textLineBuffer : Buffer<TextLineElement>).updateElement(this);
			}
		} else if (!layoutContainer.isHidden) // not full outside of the Mask anymore
		{
			#if peotelayout_debug
			// trace("showed", layoutContainer.layout.name);
			#end
			update(layoutContainer);
			show();
		}
	}

	public function setText(text:String) {
		var padChars = StringTools.rpad(text, " ", textLine.length);
		display.fontProgram.lineSetChars(textLine, padChars);
		this.display.fontProgram.lineSetPosition(textLine, x, y);
		display.fontProgram.updateLine(textLine);
	}
}
