package layoutable;

import ViewModels.LaneView;
import ViewModels.RowView;
import ViewModels.TrackView;
import elements.ElementSimple;
import ob.traxe.Lane;
import ob.traxe.LaneConfiguration.ColumnType;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Tracker;
import peote.layout.ILayoutElement;
import peote.layout.LayoutContainer;
import peote.text.Line;
import peote.view.Buffer;
import peote.view.Color;
import peote.view.Element;
import peote.view.Program;

class PatternElement implements ILayoutElement implements Element {
	@color public var c:Color = 0x55ff00d0; // using propertyname "borderColor" as identifier for setColorFormula()

	@posX public var x:Int = 0;
	@posY public var y:Int = 0;
	@sizeX @varying public var w:Int = 800;
	@sizeY @varying public var h:Int = 100;

	@zIndex public var z:Int = 0;

	@custom("maskX") @varying public var maskX:Int = 0;
	@custom("maskY") @varying public var maskY:Int = 0;
	@custom("maskWidth") @varying public var maskWidth:Int = 0;
	@custom("maskHeight") @varying public var maskHeight:Int = 0;

	var display:LayoutElementDisplay;
	var isVisible:Bool = false;

	var markerBuffer:Buffer<ElementSimple>;
	var markerProgram:Program;

	var cursorElement:ElementSimple;
	var cursorBuffer:Buffer<ElementSimple>;
	var cursorProgram:Program;

	var tracker:Tracker;
	var trackViews:Array<TrackView> = [];

	public function new(display:LayoutElementDisplay, color:Color, tracker:Tracker) {
		this.c = color;
		this.display = display;
		this.tracker = tracker;

		markerBuffer = new Buffer<ElementSimple>(16);
		markerProgram = new Program(markerBuffer);
		markerProgram.alphaEnabled = true;
		this.display.addProgram(markerProgram);

		var xOffset = 0;
		for (t in tracker.tracks) {
			var marker = new ElementSimple(xOffset, y, Theme.ColumnWidth * t.lanes.length, Theme.ColumnHeight, Theme.Edge);
			marker.z = -150;
			markerBuffer.addElement(marker);
			var trackView:TrackView = {lanes: [], playHeadIndex: 0, beatMarker: marker};
			for (lane in t.lanes) {
				trackView.lanes.push(initLaneView(lane, xOffset));
				xOffset += Std.int(lane.numColumns * Theme.ColumnWidth);
			}
			trackViews.push(trackView);
		}

		cursorBuffer = new Buffer<ElementSimple>(1);
		cursorProgram = new Program(cursorBuffer);
		cursorProgram.alphaEnabled = true;
		this.display.addProgram(cursorProgram);

		cursorElement = new ElementSimple(x, y, Theme.CursorWidth, Theme.ColumnHeight, Theme.Cursor);
		cursorBuffer.addElement(cursorElement);
	}

	// ------------------ update, show and hide ----------------------

	public inline function update(layoutContainer:LayoutContainer) {
		x = Math.round(layoutContainer.x);
		y = Math.round(layoutContainer.y);
		z = Math.round(layoutContainer.depth);
		w = Math.round(layoutContainer.width);
		h = Math.round(layoutContainer.height);
		// todo - update all lines and beat elements
		// this.display.fontProgram.lineSetPosition(textLine, x, y);
		// this.display.fontProgram.updateLine(textLine);
		if (layoutContainer.isMasked) { // if some of the edges is cut by mask for scroll-area
			maskX = Math.round(layoutContainer.maskX);
			maskY = Math.round(layoutContainer.maskY);
			maskWidth = maskX + Math.round(layoutContainer.maskWidth);
			maskHeight = maskY + Math.round(layoutContainer.maskHeight);
		} else { // if its fully displayed
			maskX = 0;
			maskY = 0;
			maskWidth = w;
			maskHeight = h;
		}
	}

	public inline function show() {
		isVisible = true;
		(display.patternBuffer : Buffer<PatternElement>).addElement(this);
	}

	public inline function hide() {
		isVisible = false;
		(display.patternBuffer : Buffer<PatternElement>).removeElement(this);
	}

	// ---------------- interface to peote-layout ---------------------

	public inline function showByLayout() {
		if (!isVisible)
			show();
	}

	public inline function hideByLayout() {
		if (isVisible)
			hide();
	}

	public inline function updateByLayout(layoutContainer:LayoutContainer) {
		// TODO: layoutContainer.updateMask() from here to make it only on-need

		if (isVisible) {
			if (layoutContainer.isHidden) // if it is full outside of the Mask (so invisible)
			{
				#if peotelayout_debug
				// trace("removed", layoutContainer.layout.name);
				#end
				hide();
			} else {
				update(layoutContainer);
				(display.patternBuffer : Buffer<PatternElement>).updateElement(this);
			}
		} else if (!layoutContainer.isHidden) // not full outside of the Mask anymore
		{
			#if peotelayout_debug
			// trace("showed", layoutContainer.layout.name);
			#end
			update(layoutContainer);
			show();
		}
	}

	function styleEmpty(rowIndex:Int):GlyphStylePacked {
		return rowIndex % 4 == 0 ? Theme.glyphStylePack(Theme.Pattern2) : Theme.glyphStylePack(Theme.Pattern3);
	}

	function styleRow(rowIndex:Int, row:{char:String, column:ColumnType}):GlyphStylePacked {
		return switch (row.char) {
			case ".": styleEmpty(rowIndex);
			case _: Theme.ColumnColors[row.column];
		}
	}

	function lineUnderCursor(cursor:NavigatorCursor):Line<GlyphStylePacked> {
		return trackViews[cursor.trackIndex].lanes[cursor.laneIndex].rows[cursor.rowIndex].line;
	}

	public function focusCursor(cursor:NavigatorCursor) {
		var line = lineUnderCursor(cursor);
		cursorElement.x = Std.int(line.x + (cursorElement.w * cursor.columnIndex));
		cursorElement.y = Std.int(line.y);
		cursorBuffer.updateElement(cursorElement);
	}

	function updateLine(rowIndex:Int, rowColumns:Array<{char:String, column:ColumnType}>, line:Line<GlyphStylePacked>) {
		for (i in 0...rowColumns.length) {
			var style = styleRow(rowIndex, rowColumns[i]);
			trace('set line char ${rowColumns[i].char}');
			display.fontProgram.lineSetChars(line, rowColumns[i].char, i, style);
		}
		display.fontProgram.updateLine(line);
	}

	function isEmptyRow(rowColumns:Array<{char:String, column:ColumnType}>):Bool {
		return rowColumns[0].char == ".";
	}

	function initRowView(lane:Lane, xOffset:Int, rowIndex:Int):RowView {
		var rowColumns = lane.formatRow(rowIndex);
		var charStyles:Array<{char:String, style:GlyphStylePacked}> = [];
		for (i => c in rowColumns) {
			charStyles.push({char: c.char, style: styleRow(i, c)});
		}
		var chars = charStyles.map(item -> item.char).join("");
		var line = display.fontProgram.createLine(chars, xOffset, Theme.ColumnHeight * rowIndex, styleEmpty(rowIndex));
		if (!isEmptyRow(rowColumns)) {
			for (charIndex => c in charStyles) {
				display.fontProgram.lineSetChars(line, c.char, charIndex, c.style);
			}
		}
		return {line: line, chars: chars};
	}

	function initLaneView(lane:Lane, xOffset:Int):LaneView {
		var rows:Array<RowView> = [];
		for (rowIndex => row in lane.rows) {
			var rowView = initRowView(lane, xOffset, rowIndex);
			rows.push(rowView);
		}
		return {rows: rows};
	}

	function clearLineChars(rowView:RowView) {
		rowView.chars = "";
		display.fontProgram.lineDeleteChars(rowView.line);
	}

	function alterTrackViewLengths(changeRowCountBy:Int, trackIndex:Int) {
		if (changeRowCountBy > 0) {
			for (laneIndex => laneView in trackViews[trackIndex].lanes) {
				var lane = tracker.tracks[trackIndex].getLane(laneIndex);
				for (rowIndex in laneView.rows.length...lane.rows.length) {
					laneView.rows.push(initRowView(lane, Std.int(trackViews[trackIndex].lanes[laneIndex].rows[rowIndex - 1].line.x), rowIndex));
				}
			}
		} else {
			for (laneIndex => laneView in trackViews[trackIndex].lanes) {
				var lane = tracker.tracks[trackIndex].getLane(laneIndex);
				for (rowIndex in lane.rows.length...laneView.rows.length) {
					clearLineChars(laneView.rows[rowIndex]);
				}
			}
		}
	}

	public function updateTrackerText(cursor:NavigatorCursor, update:LaneUpdate) {
		if (update.entireLane) {
			if (update.changeRowcountBy != 0) {
				// lane lengths are changing
				alterTrackViewLengths(update.changeRowcountBy, cursor.trackIndex);
				if (update.changeRowcountBy > 0) {
					for (rowIndex => rowView in trackViews[cursor.trackIndex].lanes[cursor.laneIndex].rows) {
						var lane = tracker.tracks[cursor.trackIndex].getLane(cursor.laneIndex);
						if (rowIndex > lane.rows.length - 1) {
							// stop updating lines because they are actually empty
							break;
						}
						var rowColumns = lane.formatRow(rowIndex);
						var line = rowView.line;
						#if debug
						trace('$rowIndex ${[for (r in rowColumns) r.char].join("")}');
						#end
						updateLine(rowIndex, rowColumns, line);
					}
				}
			} else {
				// row positions are changing
				for (rowIndex => rowView in trackViews[cursor.trackIndex].lanes[cursor.laneIndex].rows) {
					var lane = tracker.tracks[cursor.trackIndex].getLane(cursor.laneIndex);
					if (rowIndex > lane.rows.length - 1) {
						// stop updating lines because they are actually empty
						break;
					}
					var rowColumns = lane.formatRow(rowIndex);
					var line = rowView.line;
					#if debug
					trace('$rowIndex ${[for (r in rowColumns) r.char].join("")}');
					#end
					updateLine(rowIndex, rowColumns, line);
				}
			}
		} else {
			var rowIndex = cursor.rowIndex;
			var rowColumns = tracker.tracks[cursor.trackIndex].getLane(cursor.laneIndex).formatRow(rowIndex);
			var line = lineUnderCursor(cursor);
			#if debug
			trace('$rowIndex ${[for (r in rowColumns) r.char].join("")}');
			#end
			updateLine(rowIndex, rowColumns, line);
		}
	}

	public function updateMarker(trackIndex:Int) {
		trackViews[trackIndex].playHeadIndex = tracker.tracks[trackIndex].playIndex;
		trackViews[trackIndex].beatMarker.y = Std.int(trackViews[trackIndex].playHeadIndex * Theme.ColumnHeight);
		markerBuffer.updateElement(trackViews[trackIndex].beatMarker);
	}

	public function resetMarkers() {
		for (tIndex => t in tracker.tracks) {
			t.resetPlayIndex();
			trackViews[tIndex].playHeadIndex = t.playIndex;
			trackViews[tIndex].beatMarker.y = 0;
			markerBuffer.updateElement(trackViews[tIndex].beatMarker);
		}
	}
}
