package layoutable;

import peote.layout.ILayoutElement;
import peote.layout.LayoutContainer;
import peote.view.Buffer;
import peote.view.Color;
import peote.view.Element;
import peote.view.Program;

class ShapeElement implements ILayoutElement implements Element {
	@color public var c:Color = 0x55ff00d0; // using propertyname "borderColor" as identifier for setColorFormula()

	// @color("bgcolor") public var color:Color = 0xffff00ff; // using different identifier "bgcolor" for setColorFormula()
	@custom @varying public var borderRadius:Float = 1.0; // using propertyname as identifier for setColorFormula()
	@custom("borderSize") @varying public var bSize:Float = 1.0; // using different identifier "borderSize" for setColorFormula()

	@posX public var x:Int = 0;
	@posY public var y:Int = 0;
	@sizeX @varying public var w:Int = 800;
	@sizeY @varying public var h:Int = 100;

	@zIndex public var z:Int = 0;

	@custom("maskX") @varying public var maskX:Int = 0;
	@custom("maskY") @varying public var maskY:Int = 0;
	@custom("maskWidth") @varying public var maskWidth:Int = 0;
	@custom("maskHeight") @varying public var maskHeight:Int = 0;

	var display:LayoutElementDisplay;
	var isVisible:Bool = false;

	public function new(display:LayoutElementDisplay, color:Color) {
		this.c = color;
		this.display = display;
	}

	// ------------------ update, show and hide ----------------------

	public inline function update(layoutContainer:LayoutContainer) {
		x = Math.round(layoutContainer.x);
		y = Math.round(layoutContainer.y);
		z = Math.round(layoutContainer.depth);
		w = Math.round(layoutContainer.width);
		h = Math.round(layoutContainer.height);

		if (layoutContainer.isMasked) { // if some of the edges is cut by mask for scroll-area
			maskX = Math.round(layoutContainer.maskX);
			maskY = Math.round(layoutContainer.maskY);
			maskWidth = maskX + Math.round(layoutContainer.maskWidth);
			maskHeight = maskY + Math.round(layoutContainer.maskHeight);
		} else { // if its fully displayed
			maskX = 0;
			maskY = 0;
			maskWidth = w;
			maskHeight = h;
		}
	}

	public inline function show() {
		isVisible = true;
		(display.spriteBuffer : Buffer<ShapeElement>).addElement(this);
	}

	public inline function hide() {
		isVisible = false;
		(display.spriteBuffer : Buffer<ShapeElement>).removeElement(this);
	}

	// ---------------- interface to peote-layout ---------------------

	public inline function showByLayout() {
		if (!isVisible)
			show();
	}

	public inline function hideByLayout() {
		if (isVisible)
			hide();
	}

	public inline function updateByLayout(layoutContainer:LayoutContainer) {
		// TODO: layoutContainer.updateMask() from here to make it only on-need

		if (isVisible) {
			if (layoutContainer.isHidden) // if it is full outside of the Mask (so invisible)
			{
				#if peotelayout_debug
				// trace("removed", layoutContainer.layout.name);
				#end
				hide();
			} else {
				update(layoutContainer);
				(display.spriteBuffer : Buffer<ShapeElement>).updateElement(this);
			}
		} else if (!layoutContainer.isHidden) // not full outside of the Mask anymore
		{
			#if peotelayout_debug
			// trace("showed", layoutContainer.layout.name);
			#end
			update(layoutContainer);
			show();
		}
	}
}
