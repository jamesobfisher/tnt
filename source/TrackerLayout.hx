package;

import haxe.Resource;
import js.html.Response;
import js.lib.ArrayBuffer;
import lime.app.Application;
import lime.ui.Window;
import ob.seq.Synth.IPulse;
import ob.seq.Synth.Sample;
import ob.seq.WebMetronome;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Tracker;
import ob.traxe.TrackerConfiguration;
import peote.layout.ContainerType;
import peote.layout.LayoutContainer;
import peote.text.Font;
import peote.text.FontProgram;
import peote.view.PeoteView;

class TrackerLayout extends Application {
	public function new()
		super();

	public override function onWindowCreate():Void {
		switch (window.context.type) {
			case WEBGL, OPENGL, OPENGLES:
				initPeoteView(window); // start sample
			default:
				throw("Sorry, only works with OpenGL.");
		}
	}

	// ------------------------------------------------------------
	// --------------- SAMPLE STARTS HERE -------------------------
	// ------------------------------------------------------------
	var layoutContainer:LayoutContainer;
	var glyphStyle:GlyphStylePacked;
	var fontProgram:FontProgram<GlyphStylePacked>;
	var tracker:Tracker;
	var pattern:PatternView;
	var information:TextWidget;
	// var whole:TextWidget;
	// var quarter:TextWidget;
	// var sixteenth:TextWidget;
	var peoteView:PeoteView;
	#if web
	var metro:WebMetronome;
	#end
	var voices:Array<IPulse> = [];

	function scheduleTracks() {
		for (i in 0...tracker.tracks.length) {
			var r = tracker.tracks[i].getLane(0).getValue(tracker.tracks[i].playIndex);
			if (r > 0) {
				voices[i].gain = r / 140;
				#if debug
				trace('gain $i ${voices[i].gain}');
				#end
				voices[i].pulse(metro.nextNoteTime);
			}
			tracker.tracks[i].advancePlayIndex();
			pattern.updateMarker(i);
		}
	}

	public function initPeoteView(window:lime.ui.Window) {
		tracker = new Tracker(TrackerConfiguration.SevenOhSeven());
		tracker.onFocusChanged = focusCursor;
		tracker.onValueChanged = updateText;
		peoteView = new PeoteView(window);

		var packedFont = {name: "hack", range: null};

		new Font<GlyphStylePacked>('assets/fonts/packed/${packedFont.name}/config.json', packedFont.range).load(function(font) {
			glyphStyle = new GlyphStylePacked();
			glyphStyle.color = Theme.Pattern0;
			glyphStyle.width = Theme.ColumnWidth;
			glyphStyle.height = Theme.ColumnHeight;

			fontProgram = font.createFontProgram(glyphStyle);
			var display = new TextWidgetLayout(peoteView, Theme.Space);
			display.addProgram(fontProgram);

			// elements to be layed out
			information = new TextWidget(display, glyphStyle, fontProgram,
				"welcome -> cursor keys navigate, numbers edit (hex), . to erase, return to start/stop!");
			// whole = new TextWidget(display, glyphStyle, fontProgram, "0");
			// quarter = new TextWidget(display, glyphStyle, fontProgram, "0");
			// sixteenth = new TextWidget(display, glyphStyle, fontProgram, "0");
			pattern = new PatternView(display, glyphStyle, fontProgram, tracker);

			// init layout
			layoutContainer = new LayoutContainer(ContainerType.BOX, display, {}, [
				new Box(pattern, {
					width: display.width,
				}),
				new Box(information, {
					left: 0,
					width: display.width,
					height: 100,
					bottom: display.height
				})
			]);

			layoutContainer.init();

			layoutContainer.update(peoteView.width, peoteView.height);

			metro = new WebMetronome();
			metro.init();

			voices.push(new Sample(metro.audioContext, "assets/707KICK1.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707KICK2.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707SNR1.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707SNR2.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707TOML.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707TOMM.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707TOMH.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707RIM.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707COW.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707CLAP.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707TAMB.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707HHCL.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707HHOP.wav"));
			voices.push(new Sample(metro.audioContext, "assets/707CRASH1.wav"));
			// voices.push(new Sample(metro.audioContext, "assets/707CRASH2.wav"));

			// metro.onWhole = () -> whole.toggleColour();
			// metro.onQuarter = () -> quarter.toggleColour();
			metro.onSixteenth = scheduleTracks;
		}, true // debug
		);
	}

	function focusCursor(cursor:NavigatorCursor) {
		pattern.focusCursor(cursor);
		information.setText(tracker.information());
	}

	function updateText(cursor:NavigatorCursor) {
		pattern.updateTrackerText(cursor);
	}

	// ------------------------------------------------------------
	// ----------------- LIME EVENTS ------------------------------
	// ------------------------------------------------------------

	public override function onWindowResize(width:Int, height:Int):Void {
		if (layoutContainer != null)
			layoutContainer.update(width, height);
	}

	// public override function onPreloadComplete():Void {}
	// public override function update(deltaTime:Int):Void {}
	// ----------------- MOUSE EVENTS ------------------------------
	var sizeEmulation = false;

	public override function onMouseMove(x:Float, y:Float) {
		if (sizeEmulation && layoutContainer != null) {
			layoutContainer.update(x, y);
		}
	}

	// public override function onMouseDown (x:Float, y:Float, button:MouseButton) {};
	// public override function onMouseUp(x:Float, y:Float, button:MouseButton) {
	// 	sizeEmulation = !sizeEmulation;
	// 	if (sizeEmulation)
	// 		onMouseMove(x, y);
	// 	else {
	// 		layoutContainer.update(window.width, window.height);
	// 	}
	// }
	// public override function onMouseWheel (deltaX:Float, deltaY:Float, deltaMode:lime.ui.MouseWheelMode):Void {}
	// public override function onMouseMoveRelative (x:Float, y:Float):Void {}
	// ----------------- TOUCH EVENTS ------------------------------
	// public override function onTouchStart (touch:lime.ui.Touch):Void {}
	// public override function onTouchMove (touch:lime.ui.Touch):Void	{}
	// public override function onTouchEnd (touch:lime.ui.Touch):Void {}
	// ----------------- KEYBOARD EVENTS ---------------------------
	public override function onKeyDown(keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {
		#if debug
		trace('key code: ${keyCode}');
		#end
		if (keyCode == RETURN) {
			metro.toggleIsStarted();
			if (!metro.isStarted) {
				pattern.resetMarkers();
			}
		} else {
			tracker.handleKeyDown({keyCode: keyCode, modifier: modifier});
		}
	}

	// public override function onKeyUp(keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {}
	// -------------- other WINDOWS EVENTS ----------------------------
	// public override function onWindowLeave():Void { trace("onWindowLeave"); }
	// public override function onWindowActivate():Void { trace("onWindowActivate"); }
	// public override function onWindowClose():Void { trace("onWindowClose"); }
	// public override function onWindowDeactivate():Void { trace("onWindowDeactivate"); }
	// public override function onWindowDropFile(file:String):Void { trace("onWindowDropFile"); }
	// public override function onWindowEnter():Void { trace("onWindowEnter"); }
	// public override function onWindowExpose():Void { trace("onWindowExpose"); }
	// public override function onWindowFocusIn():Void { trace("onWindowFocusIn"); }
	// public override function onWindowFocusOut():Void { trace("onWindowFocusOut"); }
	// public override function onWindowFullscreen():Void { trace("onWindowFullscreen"); }
	// public override function onWindowMove(x:Float, y:Float):Void { trace("onWindowMove"); }
	// public override function onWindowMinimize():Void { trace("onWindowMinimize"); }
	// public override function onWindowRestore():Void { trace("onWindowRestore"); }
	// public override function onRenderContextLost ():Void trace(" --- WARNING: LOST RENDERCONTEXT --- ");
	// public override function onRenderContextRestored (context:lime.graphics.RenderContext):Void trace(" --- onRenderContextRestored --- ");
}
