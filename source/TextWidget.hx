package;

import elements.ElementSimple;
import peote.layout.ILayoutElement;
import peote.layout.LayoutContainer;
import peote.text.FontProgram;
import peote.text.Line;
import peote.view.Buffer;
import peote.view.Color;
import peote.view.Element;
import peote.view.Program;

class TextWidget implements ILayoutElement implements Element {
	@color("bgcolor") public var color:Color = 0x0000ffff;

	@posX public var x:Int = 0;
	@posY public var y:Int = 0;
	@sizeX @varying public var w:Int;
	@sizeY @varying public var h:Int;

	@zIndex public var z:Int = 0;

	@custom("maskX") @varying public var maskX:Int = 0;
	@custom("maskY") @varying public var maskY:Int = 0;
	@custom("maskWidth") @varying public var maskWidth:Int = 0;
	@custom("maskHeight") @varying public var maskHeight:Int = 0;

	var display:TextWidgetLayout;
	var isVisible:Bool = false;
	var glyphStyle:GlyphStylePacked;
	var toggleStyle:GlyphStylePacked;
	var fontProgram:FontProgram<GlyphStylePacked>;
	var backgroundBuffer:Buffer<ElementSimple>;
	var backgroundProgram:Program;
	var highlight:ElementSimple;
	var lines:Array<Line<GlyphStylePacked>> = [];
	var isToggled = false;

	public function new(display:TextWidgetLayout, glyphStyle:GlyphStylePacked, fontProgram:FontProgram<GlyphStylePacked>, text:String = "",
			hasCursor:Bool = false) {
		this.display = display;
		this.glyphStyle = glyphStyle;
		this.fontProgram = fontProgram;
		this.toggleStyle = Theme.glyphStylePack(Theme.Syntaxb);
		// this.fontProgram.zIndexEnabled = true;
		backgroundBuffer = new Buffer<ElementSimple>(16);
		backgroundProgram = new Program(backgroundBuffer);
		this.display.addProgram(backgroundProgram);
		var cursorWidth = hasCursor ? 14 : 0;
		var cursorHeight = hasCursor ? 28 : 0;
		highlight = new ElementSimple(x, y, cursorWidth, cursorHeight, Theme.Cursor);
		highlight.z = -100;
		backgroundBuffer.addElement(highlight);
		lines.push(fontProgram.createLine(text, x, y));
	}

	public function toggleColour() {
		isToggled = !isToggled;
		if (isToggled) {
			fontProgram.lineSetStyle(lines[0], toggleStyle);
		} else {
			fontProgram.lineSetStyle(lines[0], glyphStyle);
		}
		// trace('toggled ${isToggled}');
		fontProgram.updateLine(lines[0]);
	}

	public function setText(text:String, lineIndex:Int = 0) {
		var padChars = StringTools.rpad(text, " ", lines[lineIndex].length);
		fontProgram.lineSetChars(lines[lineIndex], padChars);
		fontProgram.updateLine(lines[lineIndex]);
	}

	function updateHighlight(x:Int, y:Int, w:Int, h:Int) {
		highlight.x = x;
		highlight.y = y;
		highlight.w = w;
		highlight.h = h;
		backgroundBuffer.updateElement(highlight);
	}

	// ------------------ update, show and hide ----------------------

	public inline function update(layoutContainer:LayoutContainer) {
		x = Math.round(layoutContainer.x);
		y = Math.round(layoutContainer.y);
		z = Math.round(layoutContainer.depth);
		w = Math.round(layoutContainer.width);
		h = Math.round(layoutContainer.height);
		fontProgram.lineSetPosition(lines[0], x, y);
		fontProgram.updateLine(lines[0]);
		if (layoutContainer.isMasked) { // if some of the edges is cut by mask for scroll-area
			maskX = Math.round(layoutContainer.maskX);
			maskY = Math.round(layoutContainer.maskY);
			maskWidth = maskX + Math.round(layoutContainer.maskWidth);
			maskHeight = maskY + Math.round(layoutContainer.maskHeight);
		} else { // if its fully displayed
			maskX = 0;
			maskY = 0;
			maskWidth = w;
			maskHeight = h;
		}
	}

	public inline function show() {
		isVisible = true;
		(display.buffer : Buffer<TextWidget>).addElement(this);
	}

	public inline function hide() {
		isVisible = false;
		(display.buffer : Buffer<TextWidget>).removeElement(this);
	}

	// ---------------- interface to peote-layout ---------------------

	public inline function showByLayout() {
		if (!isVisible)
			show();
	}

	public inline function hideByLayout() {
		if (isVisible)
			hide();
	}

	public inline function updateByLayout(layoutContainer:LayoutContainer) {
		// TODO: layoutContainer.updateMask() from here to make it only on-need

		if (isVisible) {
			if (layoutContainer.isHidden) // if it is full outside of the Mask (so invisible)
			{
				#if peotelayout_debug
				// trace("removed", layoutContainer.layout.name);
				#end
				hide();
			} else {
				update(layoutContainer);
				(display.buffer : Buffer<TextWidget>).updateElement(this);
			}
		} else if (!layoutContainer.isHidden) // not full outside of the Mask anymore
		{
			#if peotelayout_debug
			// trace("showed", layoutContainer.layout.name);
			#end
			update(layoutContainer);
			show();
		}
	}
}
