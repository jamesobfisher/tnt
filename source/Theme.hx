package;

import ob.traxe.LaneConfiguration.ColumnType;

class Theme {
	public static var CursorWidth = 14;
	public static var ColumnWidth = 28;
	public static var ColumnHeight = 28;
	public static var Pattern0 = 0x85daebFF;
	public static var Pattern1 = 0x5fc9e7FF;
	public static var Pattern2 = 0x5fa1e7FF;
	public static var Pattern3 = 0x5f6ee7FF;
	public static var Pattern4 = 0x4c60aaFF;
	public static var Edge = 0x444774FF;
	public static var EdgeAlpha = 0x444774aa;
	public static var Space = 0x32313bFF;
	public static var Cursor = 0x463c5eFF;
	public static var CursorLight = 0x5d4776FF;
	public static var Selection = 0x855395FF;
	public static var Syntax0 = 0xab58a8FF;
	public static var Syntax1 = 0xca60aeFF;
	public static var Syntax2 = 0xf3a787FF;
	public static var Syntax3 = 0xf5daa7FF;
	public static var Syntax4 = 0x8dd894FF;
	public static var Syntax5 = 0x5dc190FF;
	public static var Syntax6 = 0x4ab9a3FF;
	public static var Syntax7 = 0x4593a5FF;
	public static var Syntax8 = 0x5efdf7FF;
	public static var Syntax9 = 0xff5dccFF;
	public static var Syntaxa = 0xfdfe89FF;
	public static var Syntaxb = 0xffffffFF;
	public static var Parameter:Map<ColumnType, Int> = [
		Empty => Pattern0,
		Hex => Syntax0,
		NoteName => Syntax1,
		Accidental => Syntax2,
		Octave => Syntax3,
		Switch => Syntax4
	];

	public static function glyphStylePack(c:Int):GlyphStylePacked {
		var style = new GlyphStylePacked();
		style.width = Theme.ColumnWidth;
		style.height = Theme.ColumnHeight;
		style.color = c;
		return style;
	}

	public static var ColumnColors:Map<ColumnType, GlyphStylePacked> = [
		Empty => glyphStylePack(Theme.Pattern0),
		Hex => glyphStylePack(Theme.Syntax0),
		NoteName => glyphStylePack(Theme.Syntax1),
		Accidental => glyphStylePack(Theme.Syntax2),
		Octave => glyphStylePack(Theme.Syntax3),
		Switch => glyphStylePack(Theme.Syntax4)
	];
}
