package;

import layoutable.PatternElement;
import layoutable.ShapeElement;
import layoutable.TextLineElement;
import peote.layout.ILayoutElement;
import peote.layout.LayoutContainer;
import peote.text.FontProgram;
import peote.view.Buffer;
import peote.view.Color;
import peote.view.Display;
import peote.view.PeoteView;
import peote.view.Program;

class LayoutElementDisplay extends Display implements ILayoutElement {
	public var patternBuffer:Buffer<PatternElement>;
	public var patternProgram:Program;
	public var spriteBuffer:Buffer<ShapeElement>;
	public var spriteProgram:Program;
	public var textLineBuffer:Buffer<TextLineElement>;
	public var textLineProgram:Program;
	public var fontProgram:FontProgram<GlyphStylePacked>;

	var _peoteView:PeoteView;
	var isVisible:Bool = false;

	public function new(peoteView:PeoteView, color:Color = Color.GREY1, fontProgram:FontProgram<GlyphStylePacked>) {
		_peoteView = peoteView;
		super(0, 0, 0, 0, color);
		this.fontProgram = fontProgram;

		patternBuffer = new Buffer<PatternElement>(1);
		patternProgram = new Program(patternBuffer);
		patternProgram.alphaEnabled = true;
		addProgram(patternProgram);

		spriteBuffer = new Buffer<ShapeElement>(16, 8);
		spriteProgram = new Program(spriteBuffer);
		spriteProgram.alphaEnabled = true;
		addProgram(spriteProgram);

		textLineBuffer = new Buffer<TextLineElement>(16, 8);
		textLineProgram = new Program(textLineBuffer);
		textLineProgram.alphaEnabled = true;
		addProgram(textLineProgram);

		addProgram(fontProgram);
	}

	// ------------------ update, show and hide ----------------------

	public inline function update(layoutContainer:LayoutContainer) {
		x = Math.round(layoutContainer.x);
		y = Math.round(layoutContainer.y);

		if (layoutContainer.isMasked) { // if some of the edges is cut by mask for scroll-area
			x += Math.round(layoutContainer.maskX);
			y += Math.round(layoutContainer.maskY);
			width = Math.round(layoutContainer.maskWidth);
			height = Math.round(layoutContainer.maskHeight);
		} else {
			width = Math.round(layoutContainer.width);
			height = Math.round(layoutContainer.height);
		}
	}

	public inline function show() {
		isVisible = true;
		_peoteView.addDisplay(this);
	}

	public inline function hide() {
		isVisible = false;
		_peoteView.removeDisplay(this);
	}

	// ---------------- interface to peote-layout ---------------------

	public inline function showByLayout() {
		if (!isVisible)
			show();
	}

	public inline function hideByLayout() {
		if (isVisible)
			hide();
	}

	public function updateByLayout(layoutContainer:peote.layout.LayoutContainer) {
		// TODO: layoutContainer.updateMask() from here to make it only on-need

		if (isVisible) {
			if (layoutContainer.isHidden) // if it is full outside of the Mask (so invisible)
			{
				#if peotelayout_debug
				// trace("removed", layoutContainer.layout.name);
				#end
				hide();
			} else
				update(layoutContainer);
		} else if (!layoutContainer.isHidden) // not full outside of the Mask anymore
		{
			#if peotelayout_debug
			// trace("showed", layoutContainer.layout.name);
			#end
			update(layoutContainer);
			show();
		}
	}
}
