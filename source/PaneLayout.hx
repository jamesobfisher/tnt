package;

import layoutable.ShapeElement;
import layoutable.TextLineElement;
import lime.app.Application;
import lime.ui.MouseButton;
import lime.ui.Window;
import peote.layout.ContainerType;
import peote.layout.LayoutContainer;
import peote.layout.Size;
import peote.text.Font;
import peote.view.PeoteView;

class PaneLayout extends Application {
	public function new()
		super();

	public override function onWindowCreate():Void {
		switch (window.context.type) {
			case WEBGL, OPENGL, OPENGLES:
				initPeoteView(window); // start sample
			default:
				throw("Sorry, only works with OpenGL.");
		}
	}

	// ------------------------------------------------------------
	// --------------- SAMPLE STARTS HERE -------------------------
	// ------------------------------------------------------------
	var layoutContainer:LayoutContainer;

	public function initPeoteView(window:lime.ui.Window) {
		var peoteView = new PeoteView(window);
		var packedFont = {name: "hack", range: null};

		new Font<GlyphStylePacked>('assets/fonts/packed/${packedFont.name}/config.json', packedFont.range).load(function(font) {
			var glyphStyle = new GlyphStylePacked();
			glyphStyle.color = Theme.Pattern0;
			glyphStyle.width = Theme.ColumnWidth;
			glyphStyle.height = Theme.ColumnHeight;
			var fontProgram = font.createFontProgram(glyphStyle);
			var display = new LayoutElementDisplay(peoteView, Theme.Space, fontProgram);
			var yellowC = 0xffff0030;
			var blueC = 0x0000ff30;

			// add some graphic elements
			var background = new ShapeElement(display, 0x00000000);
			var pattern = new ShapeElement(display, 0x00000000);
			var status = new TextLineElement(display, blueC, "I am status");
			var information = new TextLineElement(display, Theme.EdgeAlpha, "I am information");

			// init a layout
			layoutContainer = new LayoutContainer(ContainerType.BOX, display, {
				#if peotelayout_debug
				name: "root",
				#end
			}, [
				// background child
				new Box(background, // same as new LayoutContainer(ContainerType.BOX, background, ...)
					{
						#if peotelayout_debug
						name: "background",
						#end
					}, [
						// pattern status and info childs
						new Box(pattern, {
							#if peotelayout_debug
							name: "pattern",
							#end
							left: 0,
						}),
						new Box(status, {
							#if peotelayout_debug
							name: "status",
							#end
							left: 0,
							height: 100,
							bottom: 0,
						}),
						new Box(information, {
							#if peotelayout_debug
							name: "info",
							#end
							width: Size.max(200),
							right: 0
						}),
					]),
			]);

			layoutContainer.init();

			layoutContainer.update(peoteView.width, peoteView.height);
		}, true);
	}

	// ------------------------------------------------------------
	// ----------------- LIME EVENTS ------------------------------
	// ------------------------------------------------------------

	public override function onWindowResize(width:Int, height:Int):Void {
		if (layoutContainer != null)
			layoutContainer.update(width, height);
	}

	// public override function onPreloadComplete():Void {}
	// public override function update(deltaTime:Int):Void {}
	// ----------------- MOUSE EVENTS ------------------------------
	var sizeEmulation = false;

	public override function onMouseMove(x:Float, y:Float) {
		if (sizeEmulation && layoutContainer != null) {
			layoutContainer.update(x, y);
		}
	}

	// public override function onMouseDown (x:Float, y:Float, button:MouseButton) {};
	public override function onMouseUp(x:Float, y:Float, button:MouseButton) {
		sizeEmulation = !sizeEmulation;
		if (sizeEmulation)
			onMouseMove(x, y);
		else {
			layoutContainer.update(window.width, window.height);
		}
	}

	// public override function onMouseWheel (deltaX:Float, deltaY:Float, deltaMode:lime.ui.MouseWheelMode):Void {}
	// public override function onMouseMoveRelative (x:Float, y:Float):Void {}
	// ----------------- TOUCH EVENTS ------------------------------
	// public override function onTouchStart (touch:lime.ui.Touch):Void {}
	// public override function onTouchMove (touch:lime.ui.Touch):Void	{}
	// public override function onTouchEnd (touch:lime.ui.Touch):Void {}
	// ----------------- KEYBOARD EVENTS ---------------------------
	// public override function onKeyDown (keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {}
	// public override function onKeyUp (keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {}
	// -------------- other WINDOWS EVENTS ----------------------------
	// public override function onWindowLeave():Void { trace("onWindowLeave"); }
	// public override function onWindowActivate():Void { trace("onWindowActivate"); }
	// public override function onWindowClose():Void { trace("onWindowClose"); }
	// public override function onWindowDeactivate():Void { trace("onWindowDeactivate"); }
	// public override function onWindowDropFile(file:String):Void { trace("onWindowDropFile"); }
	// public override function onWindowEnter():Void { trace("onWindowEnter"); }
	// public override function onWindowExpose():Void { trace("onWindowExpose"); }
	// public override function onWindowFocusIn():Void { trace("onWindowFocusIn"); }
	// public override function onWindowFocusOut():Void { trace("onWindowFocusOut"); }
	// public override function onWindowFullscreen():Void { trace("onWindowFullscreen"); }
	// public override function onWindowMove(x:Float, y:Float):Void { trace("onWindowMove"); }
	// public override function onWindowMinimize():Void { trace("onWindowMinimize"); }
	// public override function onWindowRestore():Void { trace("onWindowRestore"); }
	// public override function onRenderContextLost ():Void trace(" --- WARNING: LOST RENDERCONTEXT --- ");
	// public override function onRenderContextRestored (context:lime.graphics.RenderContext):Void trace(" --- onRenderContextRestored --- ");
}
