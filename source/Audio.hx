import ob.traxe.Audio;
import ob.traxe.Tracker;
#if web
import js.html.audio.AudioContext;
import ob.seq.Synth.Sample;
#end

class NullAudioContext implements IAudioContext {
	public function new() {
		it = null;
	}

	public var it(default, null):Dynamic;
}

#if web
class WebAudioContext implements IAudioContext {
	public function new(context:AudioContext) {
		it = context;
	}

	public var it(default, null):Dynamic;
}

class TestAudioSinks {
	public static function setup(tracker:Tracker, audioContext:AudioContext) {
		tracker.addPulseSink(new Sample(audioContext, "assets/707KICK1.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707KICK2.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707SNR1.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707SNR2.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707TOML.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707TOMM.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707TOMH.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707RIM.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707COW.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707CLAP.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707TAMB.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707HHCL.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707HHOP.wav"));
		tracker.addPulseSink(new Sample(audioContext, "assets/707CRASH1.wav"));
	}
}
#end
