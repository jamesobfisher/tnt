package;

#if web
import Audio.TestAudioSinks;
import Audio.WebAudioContext;
import js.html.audio.AudioContext;
import ob.seq.WebMetronome.WebAudioTimeContext;
#end
import Audio.NullAudioContext;
import layoutable.PatternElement;
import layoutable.ShapeElement;
import layoutable.TextLineElement;
import lime.app.Application;
import lime.ui.Window;
import ob.seq.Metronome.DeltaTimeContext;
import ob.seq.Metronome.ISchedule;
import ob.seq.Metronome.ITick;
import ob.seq.Metronome.ITimeContext;
import ob.traxe.Navigator.NavigatorCursor;
import ob.traxe.Tracker;
import ob.traxe.TrackerConfiguration;
import peote.layout.ContainerType;
import peote.layout.LayoutContainer;
import peote.text.Font;
import peote.view.PeoteView;

class TrackerPaneLayout extends Application {
	public function new()
		super();

	public override function onWindowCreate():Void {
		switch (window.context.type) {
			case WEBGL, OPENGL, OPENGLES:
				initPeoteView(window); // start sample
			default:
				throw("Sorry, only works with OpenGL.");
		}
	}

	// ------------------------------------------------------------
	// --------------- SAMPLE STARTS HERE -------------------------
	// ------------------------------------------------------------
	var layoutContainer:LayoutContainer;
	var tracker:Tracker;
	var pattern:PatternElement;
	var status:TextLineElement;
	var help:TextLineElement;
	var timeContext:ITimeContext;

	public function initPeoteView(window:lime.ui.Window) {
		var peoteView = new PeoteView(window);
		var packedFont = {name: "hack", range: null};

		new Font<GlyphStylePacked>('assets/fonts/packed/${packedFont.name}/config.json', packedFont.range).load(function(font) {
			var glyphStyle = new GlyphStylePacked();
			glyphStyle.color = Theme.Pattern0;
			glyphStyle.width = Theme.ColumnWidth;
			glyphStyle.height = Theme.ColumnHeight;
			var fontProgram = font.createFontProgram(glyphStyle);
			var display = new LayoutElementDisplay(peoteView, Theme.Space, fontProgram);

			#if !web
			timeContext = new DeltaTimeContext();
			var audioContext = new NullAudioContext();
			tracker = new Tracker(TrackerConfiguration.SevenOhSeven(), timeContext, audioContext);
			#else
			var context = new AudioContext();
			var timeContext = new WebAudioTimeContext(context);
			var audioContext = new WebAudioContext(context);
			tracker = new Tracker(TrackerConfiguration.SevenOhSeven(), timeContext, audioContext);
			TestAudioSinks.setup(tracker, audioContext.it);
			#end

			// add some graphic elements
			var background = new ShapeElement(display, 0x00000000);
			pattern = new PatternElement(display, 0x00000000, tracker);
			status = new TextLineElement(display, Theme.EdgeAlpha);
			help = new TextLineElement(display, Theme.EdgeAlpha, "Welcome (help is coming)!");

			// init a layout
			layoutContainer = new LayoutContainer(ContainerType.BOX, display, {
				#if peotelayout_debug
				name: "root",
				#end
			}, [
				// background child
				new Box(background, // same as new LayoutContainer(ContainerType.BOX, background, ...)
					{
						#if peotelayout_debug
						name: "background",
						#end
					}, [
						// pattern status and info childs
						new Box(pattern, {
							#if peotelayout_debug
							name: "pattern",
							#end
							left: 0,
						}),
						new Box(status, {
							#if peotelayout_debug
							name: "status",
							#end
							left: 0,
							height: 100,
							bottom: 0,
						}),
						new Box(help, {
							#if peotelayout_debug
							name: "info",
							#end
							width: 350,
							right: 0
						}),
					]),
			]);

			layoutContainer.init();

			layoutContainer.update(peoteView.width, peoteView.height);

			tracker.onFocusChanged = focusCursor;
			tracker.onValueChanged = updateText;
			tracker.onTrackRowChanged = pattern.updateMarker;
		}, true);
	}

	function focusCursor(cursor:NavigatorCursor) {
		pattern.focusCursor(cursor);
		status.setText(tracker.information());
	}

	function updateText(cursor:NavigatorCursor, update:LaneUpdate) {
		pattern.updateTrackerText(cursor, update);
	}

	// ------------------------------------------------------------
	// ----------------- LIME EVENTS ------------------------------
	// ------------------------------------------------------------

	public override function onWindowResize(width:Int, height:Int):Void {
		if (layoutContainer != null)
			layoutContainer.update(width, height);
	}

	public override function onKeyDown(keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {
		#if !web
		if (keyCode == ESCAPE) {
			this.window.close();
		}
		#end
		#if debug
		trace('key code: ${keyCode}');
		#end
		if (keyCode == RETURN || keyCode == TAB) {
			tracker.toggleIsStarted();
			if (!tracker.isStarted()) {
				// todo bind to event
				pattern.resetMarkers();
			}
		} else {
			tracker.handleKeyDown({keyCode: keyCode, modifier: modifier});
		}
	}

	#if !web
	public override function update(deltaTime:Int):Void {
		super.update(deltaTime);
		timeContext.update(deltaTime);
	}
	#end

	// public override function onPreloadComplete():Void {}
	// public override function update(deltaTime:Int):Void {}
	// ----------------- MOUSE EVENTS ------------------------------
	var sizeEmulation = false;
	// public override function onMouseMove(x:Float, y:Float) {}
	// public override function onMouseDown (x:Float, y:Float, button:MouseButton) {};
	// public override function onMouseUp(x:Float, y:Float, button:MouseButton) {};
	// public override function onMouseWheel (deltaX:Float, deltaY:Float, deltaMode:lime.ui.MouseWheelMode):Void {}
	// public override function onMouseMoveRelative (x:Float, y:Float):Void {}
	// ----------------- TOUCH EVENTS ------------------------------
	// public override function onTouchStart (touch:lime.ui.Touch):Void {}
	// public override function onTouchMove (touch:lime.ui.Touch):Void	{}
	// public override function onTouchEnd (touch:lime.ui.Touch):Void {}
	// ----------------- KEYBOARD EVENTS ---------------------------
	// public override function onKeyDown (keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {}
	// public override function onKeyUp (keyCode:lime.ui.KeyCode, modifier:lime.ui.KeyModifier):Void {}
	// -------------- other WINDOWS EVENTS ----------------------------
	// public override function onWindowLeave():Void { trace("onWindowLeave"); }
	// public override function onWindowActivate():Void { trace("onWindowActivate"); }
	// public override function onWindowClose():Void { trace("onWindowClose"); }
	// public override function onWindowDeactivate():Void { trace("onWindowDeactivate"); }
	// public override function onWindowDropFile(file:String):Void { trace("onWindowDropFile"); }
	// public override function onWindowEnter():Void { trace("onWindowEnter"); }
	// public override function onWindowExpose():Void { trace("onWindowExpose"); }
	// public override function onWindowFocusIn():Void { trace("onWindowFocusIn"); }
	// public override function onWindowFocusOut():Void { trace("onWindowFocusOut"); }
	// public override function onWindowFullscreen():Void { trace("onWindowFullscreen"); }
	// public override function onWindowMove(x:Float, y:Float):Void { trace("onWindowMove"); }
	// public override function onWindowMinimize():Void { trace("onWindowMinimize"); }
	// public override function onWindowRestore():Void { trace("onWindowRestore"); }
	// public override function onRenderContextLost ():Void trace(" --- WARNING: LOST RENDERCONTEXT --- ");
	// public override function onRenderContextRestored (context:lime.graphics.RenderContext):Void trace(" --- onRenderContextRestored --- ");
}
