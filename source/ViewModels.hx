import elements.ElementSimple;
import peote.text.Line;

typedef RowView = {
	chars:String,
	line:Line<GlyphStylePacked>
}

typedef LaneView = {
	rows:Array<RowView>
}

typedef TrackView = {
	lanes:Array<LaneView>,
	beatMarker:ElementSimple,
	playHeadIndex:Int
}
